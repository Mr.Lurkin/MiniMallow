//const fs = require('fs');
const Sequelize = require('sequelize');

module.exports = async (client) => {
  client.xpLockSet = new Set();
  client.user.setActivity(`${client.users.size} Marshmallows Roast | mm help`, {type: 'WATCHING', })
    .catch(console.error);
  
  console.log('All commands loaded!');
  console.log(`Preping ${client.users.size} Marshmallows for ${client.users.size} users.`);
  
  const Awake = ':blue_heart: M A R S H M A L L O W :blue_heart:';
  client.users.get('176048133438963714').send(Awake);
  client.users.get('339537241934397451').send(Awake);

  // Timer Check
  const timers = new Sequelize('database', 'username', 'password', {
    logging: false,
    storage: 'databases/timers.sqlite',
    dialect: 'sqlite',
    host: 'localhost'
  });
  const sendTable = timers.define('Messages', {
    MemberID: { type: Sequelize.STRING, unique: false, allowNull: false },
    Name: { type: Sequelize.STRING, unique: false, allowNull: false },
    TimeSet: { type: Sequelize.STRING, unique: false, allownull: false },
    RunAt: { type: Sequelize.STRING, unique: false, allowNull: false },
    Sent: { type:Sequelize.STRING, unique: false, allowNull: false },
    Content: { type: Sequelize.STRING, unique: false, allownull: false },
    RunAtMS: { type: Sequelize.STRING, unique: false, allowNull: false }},
  { timestamps: false });
  await sendTable.sync()
    .then(() => sendTable.findAll({ where: { Sent: 'false' } }).then(notSent => notSent.forEach(notSent => {
      const dateMS = new Date().getTime();
      const EmEss = notSent.dataValues.RunAtMS;
      if (EmEss < dateMS) {
        client.users.get(notSent.dataValues.MemberID).send(notSent.dataValues.Content);
        sendTable.update({ Sent: 'late' }, { where: { RunAt: notSent.dataValues.RunAt } });
      }
      if (dateMS < EmEss) {
        var time = new Date();
        var finalTime = time.setTime(notSent.dataValues.RunAtMS - dateMS);
        setTimeout(() => client.users.get(notSent.dataValues.MemberID).send(notSent.dataValues.Content), finalTime);
        setTimeout(() => sendTable.sync().then(() => {sendTable.update({ Sent: 'true' }, { where: { RunAt: notSent.dataValues.RunAt } });}), finalTime);
      }
    })));

  /*try {
    var errors = fs.readFileSync('crashreport.txt', 'utf8');
    client.users.get('176048133438963714').send('```js\n' + errors + '```');
    await fs.unlinkSync('crashreport.txt');
  } catch (error) {
    client.users.get('176048133438963714').send('There is no crash report.');
  }*/
};