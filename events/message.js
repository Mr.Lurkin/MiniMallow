module.exports = (client, message) => {
  //const Sequelize = require('sequelize');
  var fs = require('fs');
  var date = new Date();
  date = date.toLocaleString();
  const server = message.channel.type === 'text';

  // Check
  if (message.author.bot) return;

  // XP Stuff
  /*if (server) {
    if (!client.xpLockSet.has(message.author.id)) {
      var amount = [1, 2, 3, 4, 5];
      Array.prototype.randomElement = function() {
        return this[Math.floor(Math.random() * this.length)];
      };

      const xp = new Sequelize('database', 'username', 'password', {
        logging: false,
        storage: `databases/xp/${message.guild.id}.sqlite`,
        dialect: 'sqlite',
        host: 'localhost'
      });
  
      const xpTable = xp.define('Xp', {
        MemberID: { type: Sequelize.STRING, unique: true, allowNull: false },
        Name: { type: Sequelize.STRING, unique: false, allowNull: false },
        Xp: { type: Sequelize.INTEGER, unique: false, allownull: false },
        Level: { type:Sequelize.INTEGER, unique: false, allowNull: false }},
      { timestamps: false });
      xpTable.sync()
        .then(() => xpTable.findOrCreate({ where: { MemberID: message.author.id }, defaults: { Name: message.author.username, Xp: 0, Level: 0 } }).then(user => {
          xpTable.update({ Xp: user.Xp + (amount.randomElement()) }, { where: { MemberID: message.author.id } });
        }));
        
      client.xpLockSet.add(message.author.id);
      setTimeout(() => client.xpLockSet.delete(message.author.id), 60000);
      
      // XP Levels
      xpTable.sync()
        .then(() => xpTable.findOrCreate({ where: { MemberID: message.author.id }, defaults: { Name: message.author.username, Xp: 0, Level: 0 } }).then(user => {
          if (xpToLevelUp(user.dataValues.Level) < user.dataValues.Xp) {
            message.channel.send(`Congrats ${message.author.toString()} you are now level ${user.dataValues.Level +1} :tada:`);
            user.increment('level');
          }

          function xpToLevelUp(x) {
            return 5 * (10 ** -4) * ((x*100) ** 2) + (0.5 * (x*100)) + 50;
          }
        }));
    }
  }*/

  // More checks
  if (message.content.indexOf(client.config.prefix) !== 0) return;
  const args = message.content.slice(client.config.prefix.length).trim().split(/ +/g);
  const command = args.shift().toLowerCase();
  const cmd = client.commands.get(command);
  if (!cmd) return message.channel.send('That is not a command. Do mm help for a list of commands.');

  // Dm Logs
  if (message.channel.type === 'dm')
    fs.appendFileSync('logs/dmslogs.txt', `| At ${date}, ${message.author.tag} said "${message.content}" |\n`);

  // Command Log
  const user = message.mentions.users.first();
  fs.appendFileSync('logs/commandlog.txt', `| At ${date}, ${message.author.tag} used ${command}${message.mentions.users.size > 0 ? ` on <${user.tag}>` : ''}${server ? ` in ${message.guild.name}` : ' in DMs'} |\n`);
  
  cmd.run(client, message, args);
};