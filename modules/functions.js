module.exports = (client) => {
  client.loadCommand = (commandName) => {
    try {
      const props = require(`../commands/${commandName}`);
      console.log(`Loading Command: ${commandName.split('.js')[0]}`);
      if (props.init) {
        props.init(client);
      }
      client.commands.set(commandName.split('.js')[0], props);
      return false;
    } catch (e) {
      return `Unable to load command ${commandName}: ${e}`;
    }
  };
};