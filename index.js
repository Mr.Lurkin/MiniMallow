const Discord = require('discord.js');
const Enmap = require('enmap');
const fs = require('fs');
/*var date = new Date();
date = date.toLocaleString();*/

const client = new Discord.Client();
client.config = require('./config.json');
require('./modules/functions.js')(client);

fs.readdir('./events/', (err, files) => {
  if (err) return console.error(err);
  files.forEach(file => {
    const event = require(`./events/${file}`);
    const eventName = file.split('.')[0];
    client.on(eventName, event.bind(null, client));
    delete require.cache[require.resolve(`./events/${file}`)];
  });
});

client.commands = new Enmap();

fs.readdir('./commands/', (err, files) => {
  if (err) return console.error(err);
  files.forEach(file => {
    if (!file.endsWith('.js')) return console.error('Non js file found in /commands/ folder: '+file);
    client.loadCommand(file);
  });
});

/*process.on('uncaughtException', (error) => {
  fs.writeFileSync('crashreport.txt', date + ': \n' + error);
  console.error(error);
  process.exit(1);
});*/

client.login(client.config.token);