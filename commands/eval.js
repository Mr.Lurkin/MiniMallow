const Discord = require('discord.js');
const { post } = require('snekfetch');
const { inspect } = require('util');
const client = new Discord.Client();
const config = require('../config.json');
client.config = config;

exports.run = async (client, message, args) => {
  if (![client.config.ownerID, '107599228900999168', '339537241934397451'].includes(message.author.id)) return;
  const code = args.join(' ');
  const token = client.token.split('').join('[^]{0,2}');
  const rev = client.token.split('').reverse().join('[^]{0,2}');
  const filter = new RegExp(`${token}|${rev}`, 'g');
  try {
    let output = eval(code);
    if (output instanceof Promise || (Boolean(output) && typeof output.then === 'function' && typeof output.catch === 'function')) output = await output;
    output = inspect(output, { depth: 0, maxArrayLength: null });
    output = output.replace(filter, 'Fucking idiot, why are you trying to show my token? Go to the dev page, lazy ass');
    output = clean(output);
    if (output.length < 1950) {
      message.channel.send(`\`\`\`js\n${output}\n\`\`\``);
    } else {
      try {
        const { body } = await post('https://www.hastebin.com/documents').send(output);
        message.channel.send(`Output was to long so it was uploaded to hastebin https://www.hastebin.com/${body.key}.js `);
      } catch (error) {
        message.channel.send(`I tried to upload the output to hastebin but encountered this error ${error.name}:${error.message}`);
      }
    }
  } catch (error) {
    // eslint-disable-next-line no-ex-assign
    error = error.stack.split('\n');
    message.channel.send(`:x: **An error occurred:** \`${error[0]}\`\n\`\`\`\n${error[1].trim()}\n\`\`\``);
  }

  function clean(text)  {
    return text
      .replace(/`/g, '`' + String.fromCharCode(8203))
      .replace(/@/g, '@' + String.fromCharCode(8203));
  }
};

exports.help = {
  name: 'eval',
  description: '"Evaluates arbitrary javascript."',
  usage: 'eval [js code]',
  category: 'Owner'
};