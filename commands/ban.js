exports.run = async (client, message, args) => {
  const Discord = require('discord.js');
  const logChannel = client.channels.get('655201362992300033');

  if (message.channel.type !== 'text')
    return message.channel.send('This is a server only command');

  if (!message.channel.permissionsFor(message.member).toArray().includes('BAN_MEMBERS'))
    return message.channel.send('You cant ban people.');
  if (!message.channel.permissionsFor(message.guild.me).toArray().includes('BAN_MEMBERS'))
    return message.channel.send('I cant ban people.');
  if (!message.mentions.users.first())
    return message.channel.send('Please mention a user to ban');

  const bannedMember = message.mentions.members.first();
  const reason = args.splice(1).join(' ');
  if (message.author.id === bannedMember.id)
    return message.channel.send('Dont ban yourself dummy.');
  await bannedMember.send(`You were banned from ${message.guild.name}. Reason: ${reason}`);
  await bannedMember.ban().then(member => {
    message.channel.send(`${member.user.username} got banned. :no_entry_sign:`);
  });

  const embed = new Discord.RichEmbed()

    .setAuthor(message.author.username, message.author.avatarURL)
    .setColor(8840168)
    .setDescription(`**${message.author} used Ban in ${message.channel}**`)
    .setTimestamp()
    .addField('Who?', bannedMember, true);
  if (reason) embed.addField('Why?', reason, true);

  logChannel.send(embed);
};

exports.help = {
  name: 'ban',
  description: 'Bans the mentioned person',
  usage: 'ban @user [reason]',
  category: 'Staff'
};