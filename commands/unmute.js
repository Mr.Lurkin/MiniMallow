exports.run = async (client, message) => {
  const Discord = require('discord.js');
  const logChannel = client.channels.get('655201362992300033');

  if (message.channel.type !== 'text')
    return message.channel.send('This is a server only command');

  if (!message.channel.permissionsFor(message.member).toArray().includes('MANAGE_ROLES'))
    return message.channel.send('You cant unmute people.');
  if (!message.channel.permissionsFor(message.guild.me).toArray().includes('MANAGE_ROLES'))
    return message.channel.send('I cant unmute people.');
  if (!message.mentions.users.first())
    return message.channel.send('Unmute who?');

  const muteRole = message.guild.roles.find('name', 'Muted');
  const mutedMember = message.mentions.members.first();
  if (!muteRole)
    return message.channel.send('The Muted Role does not exist.');
  if (message.author.id === mutedMember.id)
    return message.channel.send('You arent muted. ||~~Obviously~~||');
  if (!mutedMember.roles.has(muteRole.id))
    return message.channel.send(`${mutedMember} isn't muted.`);

  await mutedMember.send(`You were unmuted in ${message.guild.name}.`);
  await mutedMember.removeRole(`${muteRole.id}`).then(member => {
    message.channel.send(`${member.user.username} was unmuted. :sound:`);
  });

  const embed = new Discord.RichEmbed()

    .setAuthor(message.author.username, message.author.avatarURL)
    .setColor(8840168)
    .setDescription(`**${message.author} used Unmute in ${message.channel}**`)
    .setTimestamp()
    .addField('Who?', mutedMember, true);

  logChannel.send(embed);
};

exports.help = {
  name: 'unmute',
  description: 'Unmutes the mentioned person',
  usage: 'unmute @user [reason]',
  category: 'Staff'
};