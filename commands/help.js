const Discord = require('discord.js');

exports.run = (client, message, args) => {

  var aboutHelp = new Discord.RichEmbed()
    .setTitle('Command: About')
    .setDescription('**Description:** Sends information about the mentioned person\n**Usage:** mm about @user\n**Examples:**\nmm about @Esra\nmm about me');
  var banHelp = new Discord.RichEmbed()
    .setTitle('Command: Ban')
    .setDescription('**Description:** Bans the mentioned person from the server\n**Usage:** mm ban @user [reason]\n**Examples:**\nmm ban @Kat\nmm ban @Jon Stop being a bully');
  var kickHelp = new Discord.RichEmbed()
    .setTitle('Command: Kick')
    .setDescription('**Description:** Kicks the mentioned person from the server\n**Usage:** mm kick @user [reason]\n**Examples:**\nmm kick @Pheo L e a v e');
  var muteHelp = new Discord.RichEmbed()
    .setTitle('Command: Mute')
    .setDescription('**Description:** Gives the mentioned person the mute role which stops them from messaging\n**Usage:** mm mute @user [reason]\n**Examples:**mm mute @Strawberry Stop being nsfw\n');
  var pingHelp = new Discord.RichEmbed()
    .setTitle('Command: Ping')
    .setDescription('**Description:** Sends the bot\'s ping\n**Usage:** mm ping');
  var purgeHelp = new Discord.RichEmbed()
    .setTitle('Command: Purge')
    .setDescription('**Description:** Deletes the specified amount of messages (2 to 100) \n**Usage:** mm purge [amount]\n**Examples:**\nmm purge 4');
  var unmuteHelp = new Discord.RichEmbed()
    .setTitle('Command: Unmute')
    .setDescription('**Description:** Removes the mute role off the mentioned person (If they have it)\n**Usage:** mm mute @user\n');

  if(args[0] === 'about')
    return message.channel.send(aboutHelp);
  if(args[0] === 'ban')
    return message.channel.send(banHelp);
  if(args[0] === 'kick')
    return message.channel.send(kickHelp);
  if(args[0] === 'mute')
    return message.channel.send(muteHelp);
  if(args[0] === 'ping')
    return message.channel.send(pingHelp);
  if(args[0] === 'purge')
    return message.channel.send(purgeHelp);
  if(args[0] === 'unmute')
    return message.channel.send(unmuteHelp);

  message.channel.send('The commands are: About, Ban, Kick, Mute, Ping, Purge, Unmute.\nTo find out more about each command do "mm help [command]"');
};

exports.help = {
  name: 'help',
  description: 'Help message displaying commands',
  usage: 'help [command]',
  category: 'Bot'
};