exports.run = async (client, message, args) => {
  const Discord = require('discord.js');
  const logChannel = client.channels.get('655201362992300033');
  const amountPurged = args[0];

  if (message.channel.type !== 'text')
    return message.channel.send('This is a server only command');

  if (!message.channel.permissionsFor(message.member).toArray().includes('MANAGE_MESSAGES'))
    return message.channel.send('You cant Purge messages.');
  if (!message.channel.permissionsFor(message.guild.me).toArray().includes('MANAGE_MESSAGES'))
    return message.channel.send('I cant Purge messages.');
  if (!args[0])
    return message.channel.send('How many messages?');
  await message.channel.bulkDelete(amountPurged);
  await message.channel.send('Purged ' + amountPurged + ' messages :blue_heart:');

  const embed = new Discord.RichEmbed()

    .setAuthor(message.author.username, message.author.avatarURL)
    .setColor(8840168)
    .setDescription(`**${message.author} used Purge in ${message.channel}**`)
    .setTimestamp()
    .addField('Amount:', `${amountPurged} messages purged`);

  logChannel.send(embed);
};

exports.help = {
  name: 'purge',
  description: 'Purges the stated amount of messages',
  usage: 'purge [amount]',
  category: 'Staff'
};