exports.run = async (client, message, args) => {
  const Discord = require('discord.js');
  const logChannel = client.channels.get('655201362992300033');

  if (message.channel.type !== 'text')
    return message.channel.send('This is a server only command');

  if (!message.channel.permissionsFor(message.member).toArray().includes('KICK_MEMBERS'))
    return message.channel.send('You cant Kick people.');
  if (!message.channel.permissionsFor(message.guild.me).toArray().includes('KICK_MEMBERS'))
    return message.channel.send('I cant Kick people.');
  if (!message.mentions.users.first())
    return message.channel.send('Please mention a user to kick');

  const kickedMember = message.mentions.members.first();
  const reason = args.splice(1).join(' ');
  if (message.author.id === kickedMember.id)
    return message.channel.send('Dont kick yourself dummy.');
  await kickedMember.send(`You were kicked from ${message.guild.name}. Reason: ${reason}`);
  await kickedMember.kick().then(member => {
    message.channel.send(`${member.user.username} got the boot. :boot:`);
  });

  const embed = new Discord.RichEmbed()

    .setAuthor(message.author.username, message.author.avatarURL)
    .setColor(8840168)
    .setDescription(`**${message.author} used Kick in ${message.channel}**`)
    .setTimestamp()
    .addField('Who?', kickedMember, true);
  if (reason) embed.addField('Why?', reason, true);

  logChannel.send(embed);
};

exports.help = {
  name: 'kick',
  description: 'Kicks the mentioned person from the server',
  usage: 'kick @user [reason]',
  category: 'Staff'
};