exports.run = async (client, message, args) => {
  const Discord = require('discord.js');
  const logChannel = client.channels.get('655201362992300033');

  if (message.channel.type !== 'text')
    return message.channel.send('This is a server only command');

  if (!message.channel.permissionsFor(message.member).toArray().includes('MANAGE_ROLES'))
    return message.channel.send('You cant mute people.');
  if (!message.channel.permissionsFor(message.guild.me).toArray().includes('MANAGE_ROLES'))
    return message.channel.send('I cant mute people.');
  if (!message.mentions.users.first())
    return message.channel.send('Mute who?');

  const muteRole = message.guild.roles.find('name', 'Muted');
  const mutedMember = message.mentions.members.first();
  if (!muteRole)
    return message.channel.send('The Muted Role does not exist.');
  if (message.author.id === mutedMember.id)
    return message.channel.send('I dont think you want to mute yourself.');
  if (mutedMember.roles.has(muteRole.id))
    return message.channel.send(`${mutedMember} is already muted.`);
  
  const reason = args.splice(1).join(' '); 
  await mutedMember.send(`You were muted in ${message.guild.name}. Reason: ${reason}`);
  await mutedMember.addRole(`${muteRole.id}`).then(member => {
    message.channel.send(`${member.user.username} got muted. :mute:`);
  });

  const embed = new Discord.RichEmbed()

    .setAuthor(message.author.username, message.author.avatarURL)
    .setColor(8840168)
    .setDescription(`**${message.author} used Mute in ${message.channel}**`)
    .setTimestamp()
    .addField('Who?', mutedMember, true);
  if (reason) embed.addField('Why?', reason, true);

  logChannel.send(embed);
};

exports.help = {
  name: 'mute',
  description: 'Mutes the mentioned person',
  usage: 'mute @user [reason]',
  category: 'Staff'
};