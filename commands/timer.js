var parse = require('parse-duration');
var moment = require('moment');
var Sequelize = require('sequelize');

exports.run = async (client, message, args) => {
  const Strwbry = client.users.get('339537241934397451');
  if (message.author.id === client.config.ownerID || message.author.id === Strwbry);

  if(!args[0])
    return message.channel.send('How long?');

  const content = args.splice(1).join(' ');
  const toSend = content ? content : 'Times up';
  const waitTime = parse(args[0]);
  var date = new Date();
  date = date.toLocaleString();
  const currentTime = moment(date).format('MM/DD/YYYY h:mm:ss a');
  var dateMS = new Date();
  var finalMS = dateMS.setTime(dateMS.getTime() + waitTime); 
  var momentified = moment(finalMS).format('MM/DD/YYYY h:mm:ss a');

  const timers = new Sequelize('database', 'username', 'password', {
    logging: false,
    storage: 'databases/timers.sqlite',
    dialect: 'sqlite',
    host: 'localhost'
  });
  const sendTable = timers.define('Messages', {
    MemberID: { type: Sequelize.STRING, unique: false, allowNull: false },
    Name: { type: Sequelize.STRING, unique: false, allowNull: false },
    TimeSet: { type: Sequelize.STRING, unique: false, allownull: false },
    RunAt: { type: Sequelize.STRING, unique: false, allowNull: false },
    Sent: { type:Sequelize.STRING, unique: false, allowNull: false },
    Content: { type: Sequelize.STRING, unique: false, allownull: false },
    RunAtMS: { type: Sequelize.STRING, unique: false, allowNull: false }},
  { timestamps: false });
  sendTable.sync()
    .then(() => sendTable.create({ MemberID: message.author.id, Name: message.author.username, TimeSet: currentTime, RunAt: momentified, Sent: 'false', Content: toSend, RunAtMS: finalMS}));

  message.channel.send('Timer Set');
  setTimeout(() => message.author.send(toSend), waitTime);
  setTimeout(() => sendTable.sync().then(() => sendTable.findOne({ where: { RunAt: momentified } }).then(() => {sendTable.update({ Sent: 'true' }, { where: { RunAt: momentified } });})), waitTime);
};