const Discord = require('discord.js');

exports.run = async (client, message) => {
  const helpChannel = client.channels.get('655535355151253508');
  const Strwbry = client.users.get('339537241934397451');
  if (message.author.id === client.config.ownerID || message.author.id === Strwbry);
  
  const info = new Discord.RichEmbed()

    .setTitle('**Mini Mallow**')
    .setThumbnail(client.user.displayAvatarURL)
    .setColor(8840168)
    .setDescription('I am a bot made specifically for Marshmallow and Music.\nThere are a few staff commands that log to <#655201362992300033>');

  const commands = new Discord.RichEmbed()

    .setTitle('**MiniMallow**\n\nStaff Only Commands Help')
    .setDescription('Command Name\nWhat it does\nUsage')
    .setColor(8840168)
    .addField('Ban', 'Bans a user\nban @user [reason]', true)
    .addField('Kick', 'Kicks a user\nkick @user [reason]', true)
    .addField('Mute', 'Mutes a user\nmute @user [reason]', true)
    .addField('Purge', 'Purges commands\npurge [amount]', true)
    .addField('Unmute', 'Unmutes a muted user\nunmute @user', true);

  await helpChannel.bulkDelete(5); 
  await helpChannel.send(info);
  await helpChannel.send(commands);
};