exports.run = async (client, message) => {
  const Strwbry = client.users.get('339537241934397451');
  if (message.author.id === client.config.ownerID || message.author.id === Strwbry);
  await message.channel.send('Going to sleep :zzz:');
  await require('child_process').execSync('pm2 kill');
};

exports.help = {
  name: 'kill',
  description: 'Sends the bot offline',
  usage: 'kill',
  category: 'Owner'
};