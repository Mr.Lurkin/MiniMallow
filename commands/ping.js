const Discord = require('discord.js');

exports.run = (client, message) => {
  const ping = `${Math.round(client.ping)}ms`;
  
  const embed = new Discord.RichEmbed()

    .setColor(8840168)
    .setTimestamp()
    .setThumbnail('https://cdn.discordapp.com/attachments/473467714858516481/551351387984887823/pingpongsvg.png')
    .addField('Pong', ping, true);

  message.channel.send({embed});
};

exports.help = {
  name: 'ping',
  description: 'Shows the bots ping',
  usage: 'ping',
  category: 'Bot'
};