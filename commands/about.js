const Discord = require('discord.js');
//const Sequelize = require('sequelize');
var moment = require('moment');

exports.run = (client, message, args) => {

  if (message.channel.type !== 'text')
    return message.channel.send('This is a server only command');

  /*const xp = new Sequelize('database', 'username', 'password', {
    logging: false,
    storage: `databases/xp/${message.guild.id}.sqlite`,
    dialect: 'sqlite',
    host: 'localhost'
  });

  const xpTable = xp.define('Xp', {
    MemberID: { type: Sequelize.STRING, unique: true, allowNull: false },
    Name: { type: Sequelize.STRING, unique: false, allowNull: false },
    Xp: { type: Sequelize.INTEGER, unique: false, allownull: false },
    Level: { type:Sequelize.INTEGER, unique: false, allowNull: false }},
  { timestamps: false });*/

  const pinged = message.mentions.users.first();
  const member = message.mentions.users.size === 0 ? message.author : message.mentions.users.first();
  const user = member;
  const userJoined = moment(user.joinedTimestamp).format('MMM Do YYYY, h:mm a');
  const userCreated = moment(user.createdTimestamp).format('MMM Do YYYY, h:mm a');
  const findStatus = user.presence.status === 'online' ? '<:online:657711635787546625>' : user.presence.status === 'dnd' ? '<:dnd:657711635997261844>' : user.presence.status === 'idle' ? '<:idle:657711636026490890> ' : user.presence.status === 'offline' ? '<:invisible:657711636328611861>' : '<:streaming:657711636034879488>';

  const embed = new Discord.RichEmbed()

    .setAuthor(user.tag, user.displayAvatarURL)
    .setThumbnail(user.displayAvatarURL)
    .setDescription(findStatus + ' - ' + user.toString() + ' - ' + user.id);
    
  /*xpTable.sync()
    .then(() => xpTable.findOne({ where: { MemberID: user } }).then(user => {
      const userLevel = user.dataValues.Level;
      const userXp = user.dataValues.Xp;
      const xpTillLvlUp = xpToLevelUp(userLevel);

      embed.addField('Level', userLevel, true);
      embed.addField('XP', `${userXp}/${xpTillLvlUp}`);

      function xpToLevelUp(x) {
        return 5 * (10 ** -4) * ((x*100) ** 2) + (0.5 * (x*100)) + 100;
      }
    }));*/

  if(user.presence.game && !user.presence.game.streaming) embed.addField('Playing', user.presence.game.name, true);
  if(user.presence.game && user.presence.game.streaming) embed.addField('Streaming', `[${user.presence.game.name}](${user.presence.game.url})`);
  if(message.channel.type === 'text') embed.addField('Joined', userJoined, true);
  embed.addField('Registered', userCreated, true);
  
  if (pinged || args[0] === 'me')
    return message.channel.send(embed).catch((e) => {
      message.channel.send(e);
    });

  message.channel.send('About who? :confused:');
};

exports.help = {
  name: 'about',
  description: 'Sends a person\'s information',
  usage: 'about @user',
  category: 'Server'
};