exports.run = async (client, message) => {
  const Strwbry = client.users.get('339537241934397451');
  if (message.author.id === client.config.ownerID || message.author.id === Strwbry);
  await message.channel.send('*Restarting. Please Wait...*');
  await console.log('Restarting...');
  await require('child_process').execSync('pm2 restart index.js --attach');
};

exports.help = {
  name: 'restart',
  description: 'Restarts the bot',
  usage: 'restart',
  category: 'Bot'
};