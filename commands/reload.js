exports.run = (client, message, args) => {
  const Strwbry = client.users.get('339537241934397451');
  if (message.author.id === client.config.ownerID || message.author.id === Strwbry);
  if (!args[0] || args.size < 1) return message.channel.send('Must provide a command name to reload.');
  const commandName = args[0];
  if (!client.commands.has(commandName)) {
    return message.channel.send('That command does not exist');
  }
  delete require.cache[require.resolve(`./${commandName}.js`)];
  client.commands.delete(commandName);
  const props = require(`./${commandName}.js`);
  client.commands.set(commandName, props);
  message.channel.send(`The ${commandName} command has been reloaded`);
  console.log(`Reloaded ${commandName}`);
};

exports.help = {
  name: 'reload',
  description: 'Reloads a command (Puts new code into effect)',
  usage: 'reload [command name]',
  category: 'Bot'
};